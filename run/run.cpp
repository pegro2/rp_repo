#include <iostream>
#include <cassert>

/* include ba_graph */
#include "../implementation.h"
#include <io.hpp>

/* include our library */
#define DEBUG
#include "../project.hpp"

/* only graph of colorings will be printed, not its interpretation */
// comment out if you wish to see full graph output
#define PRINT_GRAPH_ONLY

int main()
{
	/*
	 * Read input until EOF
	 * For every g6-string read, create ColoringGraph of given graph
	 * Then
	 *   To stderr print order,no_components
	 *   To stdout print coloringgraph
	 **/
	for( std::string g6; std::getline( std::cin, g6 ); )
	{
		const CubicMultipole m( g6 );
		const ColoringGraph clrgraph( m );

		std::cerr << g6 << ' '
			  << clrgraph.order() << ' '
			  << clrgraph.no_components() << '\n';

		std::cout << g6 << '\n'
#ifdef PRINT_GRAPH_ONLY
			  << clrgraph.print_graph();
#else
			  << clrgraph.print();
#endif
	}
	return 0;
}
