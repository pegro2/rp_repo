#!/bin/sh
OUTPUT_DIR=output


#
# arg1=directory in which to summarize
# arg2={no_components|order} attribute 
#
summarize(){ 
	OUTFILE=$OUTPUT_DIR/$1/summary.$2.csv

	#
	# empty it
	#
	rm -f $OUTFILE

	#
	# Add header (sed ... print every 6th)
	#
	echo -n "cub<order>.<girth> \\ $2," > $OUTFILE
	if [ "$2" = "order" ]
	then
		perl -e "my @a=(0..$3); for(@a){ print(\"\$_,\"); }" \
		| sed -E -e 's/([^,]*),[^,]*,[^,]*,[^,]*,[^,]*,[^,]*,/\1,/g' \
		>> $OUTFILE
	else
		perl -e "my @a=(0..$3); for(@a){ print(\"\$_,\"); }" >> $OUTFILE
	fi
	echo >> $OUTFILE

	

	#
	# fill it with csv data:
	#   COLUMN number == value of attribute
	# sed because (print every 6th)
	#
	for FILE in $OUTPUT_DIR/$1/*.summary.$2
	do
		BASENAME=$( basename $FILE | cut -d '.' -f 1,2 )
		echo -n "$BASENAME," >>$OUTFILE

		if [ "$2" = "order" ]
		then
			./to_csv.out  <$FILE 1 \
			| sed -E -e 's/([^,]*),[^,]*,[^,]*,[^,]*,[^,]*,[^,]*,/\1,/g' \
			>>$OUTFILE
		else
			./to_csv.out  <$FILE 1  >>$OUTFILE
		fi
	done


}
#
# arg1=directory
#
parse(){

	MAX_ORDER=0
	MAX_NO_COMPONENTS=0
	for FILE in graphs/$1/*
	do
		FNAME=$(basename $FILE)
		OUTFILE=$OUTPUT_DIR/$1/$FNAME



		# 
		# 1 remove /usr/bin/time info
		#   --head -n -X wih negative number should print all BUT last X lines
		# 2 cut out what you are interested in
		# 3 count same lines
		#   --sort | uniq
		# 
		head -n -2 $OUTFILE.stats | cut -d ' ' -f 2,3 | sort -n | uniq -c | cat > $OUTFILE.summary
		head -n -2 $OUTFILE.stats | cut -d ' ' -f 2 | sort -n | uniq -c | cat > $OUTFILE.summary.order
		head -n -2 $OUTFILE.stats | cut -d ' ' -f 3 | sort -n | uniq -c | cat > $OUTFILE.summary.no_components



		#
		# find out max_order, max_no_components .. to be able to build a header
		#
		NOW_MAX_ORDER=$( tail -n 1 $OUTFILE.summary.order | grep -o "[0-9]*$" | cat )
		if [ $NOW_MAX_ORDER -gt $MAX_ORDER ]
		then
			MAX_ORDER=$NOW_MAX_ORDER
		fi
		NOW_MAX_NO_COMPONENTS=$( tail -n 1 $OUTFILE.summary.no_components | grep -o "[0-9]*$" | cat )
		if [ $NOW_MAX_NO_COMPONENTS -gt $MAX_NO_COMPONENTS ]
		then
			MAX_NO_COMPONENTS=$NOW_MAX_NO_COMPONENTS
		fi

		echo "DONE $FNAME" >&2
        done

	summarize $1 no_components $MAX_NO_COMPONENTS
	summarize $1 order $MAX_ORDER

	echo "FINISHED graphs/$1" >&2
}


#
# Script body
#
g++ -O2 to_csv.cpp -o to_csv.out
if [ $? -eq 0 ]
then
	echo "COMPILATION OK"

	#
	# remove old versions
	#
	rm -f $OUTPUT_DIR/gsummary.order.csv
	rm -f $OUTPUT_DIR/gsummary.no_components.csv


	#
	# parse & summarize
	#
	for DIR in girth3 girth4 girth5 girth6
	do
		#
		# actual parsing
		#
		parse $DIR

		#
		# all stats in one file
		#
		echo "" >> $OUTPUT_DIR/gsummary.order.csv
		cat $OUTPUT_DIR/$DIR/summary.order.csv >> $OUTPUT_DIR/gsummary.order.csv
		echo "" >> $OUTPUT_DIR/gsummary.no_components.csv
		cat $OUTPUT_DIR/$DIR/summary.no_components.csv >> $OUTPUT_DIR/gsummary.no_components.csv
	done

else
	echo "COMPILATION FAILED"
fi
