#include <iostream>
#include <sstream>

#include <vector>

int main()
{
	std::vector<size_t> table;
	ssize_t max_attribute = -1;

	//
	// Read input building table
	//
	for( std::string line; std::getline( std::cin, line ); )
	{
		std::stringstream ss(line);

		unsigned long long count;
		ssize_t attribute; /*attribute=no_components|order*/
		ss >> count >> attribute;

		if( attribute > max_attribute )
		{
			max_attribute = attribute;
			table.resize( max_attribute+1 );
		}
		table[attribute] += count;
	}

	//
	// Output csv
	//
	std::stringstream ss_count;
	for( ssize_t i=0;  i<=max_attribute  ; ++i )
	{
		ss_count << table[i] << ',';
	}
	ss_count << '\n';

	std::cout << ss_count.str();
	return 0;
}
