#!/bin/sh

OUTPUT_DIR=output
RUN=./run.out
run(){

	for FILE in graphs/$1/*
	do
		FNAME=$(basename $FILE)
		#echo $FILE;
		#echo $FNAME
		#echo "./run.out <$FILE 1>$OUTPUT_DIR/$1/$FNAME.graph 2>$OUTPUT_DIR/$1/$FNAME.stats"
		/usr/bin/time $RUN <$FILE 1>$OUTPUT_DIR/$1/$FNAME.graph 2>$OUTPUT_DIR/$1/$FNAME.stats
		echo "DONE $FNAME"
        done
	echo "FINISHED graphs/$1"
}

if [ -f $RUN ]
then
	for DIR in girth3 girth4 girth5 girth6
	do
		mkdir -p $OUTPUT_DIR/$DIR/
		run $DIR &
	done
else
	echo "FAIL: need to compile run.out ... make run"
fi
