#BA_GRAPH_INCLUDE = $${HOME}/lib/ba-graph/master/include
BA_GRAPH_MASTER = $${HOME}/lib/ba-graph/master

CFLAGS = -std=c++17 -fconcepts -I$(BA_GRAPH_MASTER)/include
LDFLAGS = -lstdc++fs
DBGFLAGS = -g -O0 -Wpedantic -Wall -Wextra -Wno-unused-parameter
RUNFLAGS = -g -O2 -Wpedantic -Wall -Wextra -Wno-unused-parameter
COMPILE_DBG = $(CXX) $(CFLAGS) $(DBGFLAGS)
COMPILE_RUN = $(CXX) $(CFLAGS) $(RUNFLAGS)
###


TEST_DIR = test
TESTS_TO_RUN := $(patsubst $(TEST_DIR)/test_%.cpp, %, $(wildcard $(TEST_DIR)/test_*.cpp))
DEEP_TEST_DIR = test_deep
DEEP_TESTS_TO_RUN := $(patsubst $(DEEP_TEST_DIR)/test_%.cpp, %, $(wildcard $(DEEP_TEST_DIR)/test_*.cpp))

all: test

test: $(TESTS_TO_RUN)
test_deep: $(DEEP_TESTS_TO_RUN)

run_safe:
	$(COMPILE_DBG) run/run.cpp -o run/run.out $(LDLIBS)
run:
	$(COMPILE_RUN) run/run.cpp -o run/run.out $(LDLIBS)

implementation_basic:
	cp $(BA_GRAPH_MASTER)/test/implementation_basic.h implementation.h

$(TESTS_TO_RUN): %:
	@echo "------------------------------ $% ------------------------------"
	$(COMPILE_DBG) $(TEST_DIR)/test_$*.cpp -o $(TEST_DIR)/$*.out $(LDLIBS)
	$(TEST_DIR)/$*.out

$(DEEP_TESTS_TO_RUN): %:
	@echo "------------------------------ $% ------------------------------"
	$(COMPILE_DBG) $(DEEP_TEST_DIR)/test_$*.cpp -o $(DEEP_TEST_DIR)/$*.out $(LDLIBS)
	valgrind $(DEEP_TEST_DIR)/$*.out

clean:
	rm -rf *.out
	rm -rf $(TEST_DIR)/*.out
	rm -rf $(DEEP_TEST_DIR)/*.out


.PHONY: all clean implementation_basic test test_deep run run_safe
