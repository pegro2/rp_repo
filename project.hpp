#ifndef RP_PROJECT_HPP
#define RP_PROJECT_HPP

/* Header that includes EVERYTHING in this project */

#include "project/types.hpp"

#include "project/multipol.hpp"
#include "project/multipol.cpp"

#include "project/coloring.hpp"
#include "project/coloring.cpp"

#include "project/colorgen.hpp"
//#include "project/coloreqv.hpp"


#include "project/clrgraph.hpp"
#include "project/clrgraph.cpp"

#endif
