#include <iostream>
#include <cassert>

#include "../implementation.h"
#include <io.hpp>

#define DEBUG
#define DUMMY_COLORING
#include "../project.hpp"
#include "coloreqv.hpp"

#define CHECK(expr,counter) \
	if(expr){++(counter);} \
	else{std::cerr << "[FAIL]:" #expr "\n";assert(0);}

inline static void
test_graph_colorings( std::string, const std::vector<std::string>& );

int main()
{
	std::stringstream ss;

	/********************** SMALL GRAPH *********************/
	std::string str6_3{ "E{Sw" }; /*girth:3 vertices:6 */
	std::vector<std::string> expected_str6_3;

	/* Doing this only once, later we only test color after normalization */
	ss.str(std::string());
	ss  <<  "6\n"
	    <<  "1  2  3\n"
	    <<  "1  3  2\n"
	    <<  "2  3  1\n"
	    <<  "3  1  2\n"
	    <<  "2  1  3\n"
	    <<  "1  2  3\n";
	expected_str6_3.push_back (ss.str() );
	ss.str(std::string());
	ss  <<  "6\n"
	    <<  "1  3  2\n"
	    <<  "1  2  3\n"
	    <<  "3  2  1\n"
	    <<  "2  1  3\n"
	    <<  "3  1  2\n"
	    <<  "1  3  2\n";
	expected_str6_3.push_back (ss.str() );
	ss.str(std::string());
	ss  <<  "6\n"
	    <<  "2  1  3\n"
	    <<  "2  3  1\n"
	    <<  "1  3  2\n"
	    <<  "3  2  1\n"
	    <<  "1  2  3\n"
	    <<  "2  1  3\n";
	expected_str6_3.push_back (ss.str() );
	ss.str(std::string());
	ss  <<  "6\n"
	    <<  "2  3  1\n"
	    <<  "2  1  3\n"
	    <<  "3  1  2\n"
	    <<  "1  2  3\n"
	    <<  "3  2  1\n"
	    <<  "2  3  1\n";
	expected_str6_3.push_back (ss.str() );
	ss.str(std::string());
	ss  <<  "6\n"
	    <<  "3  1  2\n"
	    <<  "3  2  1\n"
	    <<  "1  2  3\n"
	    <<  "2  3  1\n"
	    <<  "1  3  2\n"
	    <<  "3  1  2\n";
	expected_str6_3.push_back (ss.str() );
	ss.str(std::string());
	ss  <<  "6\n"
	    <<  "3  2  1\n"
	    <<  "3  1  2\n"
	    <<  "2  1  3\n"
	    <<  "1  3  2\n"
	    <<  "2  3  1\n"
	    <<  "3  2  1\n";

	expected_str6_3.push_back( ss.str() );

	//test_graph_colorings( str6_3, expected_str6_3 );
	/* We test manually the correct count and order of colorings */
	CubicMultipole m( str6_3 );
	std::unique_ptr< const std::vector<Coloring>> cols = all_colorings( m );

	for( int i=0; i<6; ++i ){
		Coloring expected_col = Coloring(expected_str6_3[i]);
		assert( color_identical( (*cols)[i], expected_col) );
	}
	std::cout << "[OK] Simple Coloring g6='" << str6_3 << "'\n";



	/******************* TWISTED CUBE ***********************/
	std::string str_twistedCube{"GKh]@c"};
	std::vector<std::string> expected_twistedCube;

	ss.str(std::string());
	ss  << "8\n"
	    << "1  2  3\n"
	    << "1  3  2\n"
	    << "1  3  2\n"
	    << "1  2  3\n"
 	    << "2  3  1\n"
	    << "3  2  1\n"
	    << "3  2  1\n"
	    << "2  3  1\n";
	expected_twistedCube.push_back (ss.str() );
	ss.str(std::string());
	ss  << "8\n"
	    << "1  2  3\n"
	    << "3  1  2\n"
	    << "3  1  2\n"
	    << "1  2  3\n"
 	    << "2  1  3\n"
	    << "1  2  3\n"
	    << "3  2  1\n"
	    << "2  3  1\n";
	expected_twistedCube.push_back (ss.str() );
	ss.str(std::string());
	ss  << "8\n"
	    << "1  2  3\n"
	    << "2  3  1\n"
	    << "2  3  1\n"
	    << "1  2  3\n"
 	    << "2  3  1\n"
	    << "3  2  1\n"
	    << "3  1  2\n"
	    << "1  3  2\n";
	expected_twistedCube.push_back (ss.str() );


	test_graph_colorings( str_twistedCube, expected_twistedCube);
	std::cout << "[OK] TwistedCube Coloring g6='" << str_twistedCube << "'\n";

	/************************* SNARK ************************/
	/* some snark(girth 5+, sz 22): https://hog.grinvin.org/Snarks */
	std::string snark22_5{ "U?hW@eOGG?GA_A?_g???@?B??@_C?_??S?GO??@W" };
	std::vector<std::string> expected_snark;

	test_graph_colorings( snark22_5, expected_snark );
	std::cout << "[OK] Snark Coloring g6='" << snark22_5 << "'\n";


	/*********************** A BIG SNARK ********************/
	/* some snark(girth 6+, sz 38): https://hog.grinvin.org/Snarks */
	std::string snark38_6{ "e?gA@eOOGC?A???Bg?Og@_A???H?G???CG????`GG?????????O??@????C????A@???_???DG??A?????@c???K?????_G???P?????_O????CC?????B_" };

	test_graph_colorings( snark38_6, expected_snark );
	std::cout << "[OK] Snark Coloring g6='" << snark38_6 << "'\n";

	/************************* THE END ***********************/
	std::cout << "[OK] Coloring generation\n";
	return 0;
}

inline static void
test_graph_colorings( std::string g6, const std::vector<std::string>& expected )
{
	//ba_graph::Graph g( ba_graph::read_graph6(g6.begin(), g6.end()) );
	//std::cout << print_nice( g );


	/*Compute expected Colorings*/
	std::vector<Coloring> expected_cols;
	std::vector<int> counts;
	for( auto ba_literal : expected )
	{
		expected_cols.push_back( Coloring(ba_literal) );
		counts.push_back(6);
	}


	/*Compute colorings*/
	CubicMultipole m( g6 );
	std::unique_ptr< const std::vector<Coloring>> cols = all_colorings( m );



	/*count matches: we are looking for color-equvalent graph coloreqv.hpp*/
	for( const Coloring& col : *cols )
	{
		bool is_equivalent_to_some=false;
		for( size_t i=0 ; i<expected.size() ; ++i )
		{

			if( color_equivalent_with( col, expected_cols[i]) )
			{
				--counts[i];
				is_equivalent_to_some=true;
			}
		}
		assert(is_equivalent_to_some);
	}


	//Verify that every expected Coloring was found EXACTLY 6(3!) times
	for( int& count : counts )
	{
		assert( count==0 );
	}
}
