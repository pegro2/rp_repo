#include <iostream>
#include <cassert>

/* include ba_graph */
#include "../implementation.h"
#include <io.hpp>

/* include our library */
#define DEBUG
#include "../project.hpp"


/* this-test specific */
#include <sstream>
#include <set>
#include <map>

int main(){

	std::set< Coloring > mnozina;
	std::map< Coloring, int > mapa;

	const std::string str_twistedCube{"GKh]@c"};
	const CubicMultipole m_twistedCube( str_twistedCube );

	std::stringstream ss;
	ss.str(std::string());
	ss  << "8\n"
	    << "1  2  3\n"
	    << "1  3  2\n"
	    << "1  3  2\n"
	    << "1  2  3\n"
 	    << "2  3  1\n"
	    << "3  2  1\n"
	    << "3  2  1\n"
	    << "2  3  1\n";
	Coloring col1( m_twistedCube, ss.str() );
	assert( ss.str() == col1.print() );

	ss.str(std::string());
	ss  << "8\n"
	    << "1  2  3\n"
	    << "3  1  2\n"
	    << "3  1  2\n"
	    << "1  2  3\n"
 	    << "2  1  3\n"
	    << "1  2  3\n"
	    << "3  2  1\n"
	    << "2  3  1\n";
	Coloring col2( m_twistedCube, ss.str() );
	assert( ss.str() == col2.print() );

	ss.str(std::string());
	ss  << "8\n"
	    << "1  2  3\n"
	    << "2  3  1\n"
	    << "2  3  1\n"
	    << "1  2  3\n"
 	    << "2  3  1\n"
	    << "3  2  1\n"
	    << "3  1  2\n"
	    << "1  3  2\n";
	Coloring col3( m_twistedCube, ss.str() );
	assert( ss.str() == col3.print() );
	std::cout << "[OK] I/O ba_color\n";


	/* correct color */
	assert(  col3[0][0] == Color::_01  );
	assert(  col3[0][1] == Color::_10  );
	assert(  col3[0][2] == Color::_11  );
	assert(  col3[1][0] == Color::_10  );
	assert(  col3[1][1] == Color::_11  );
	assert(  col3[1][2] == Color::_01  );

	assert(  col3(0,3) == Color::_01  );
	assert(  col3(0,4) == Color::_10  );
	assert(  col3(0,6) == Color::_11  );
	std::cout << "[OK] Correct color accessed via op[] and op() \n";


	assert(  mnozina.insert( col1 ).second == true  );
	assert(  mnozina.insert( col2 ).second == true  );
	assert(  mnozina.insert( col3 ).second == true  );
	assert(  mnozina.insert( col1 ).second == false  );
	assert(  mnozina.count( col1 ) == 1  );
	assert(  mnozina.count( col2 ) == 1  );
	assert(  mnozina.count( col3 ) == 1  );


	mapa[col1] = 1;
	mapa[col2] = 2;
	mapa[col3] = 3;
	assert(  mapa[col2] == 2  );
	assert(  mapa[col3]++ == 3  );
	assert(  mapa[col3] == 4  );
	std::cout << "[OK] std::set<Coloring>, std::map<Coloring,int>\n";



	//std::cout << clr.multipole().print();
	/* TwistedCube:
		8\n"
		3  4  6\n"
		2  5  6\n"
		1  4  7\n"
		0  5  7\n"
		0  2  5\n"
		1  3  4\n"
		0  1  7\n"
		2  3  6\n"
	   Coloring1:
	    << "8\n"
	    << "1  2  3\n"
	    << "1  3  2\n"
	    << "1  3  2\n"
	    << "1  2  3\n"
 	    << "2  3  1\n"
	    << "3  2  1\n"
	    << "3  2  1\n"
	    << "2  3  1\n";

	*/

	/* next() */
	assert(  col1.next(0,Color::_01) == 3  );
	assert(  col1.next(0,Color::_10) == 4  );
	assert(  col1.next(0,Color::_11) == 6  );
	assert(  col1.next(1,Color::_01) == 2  );
	assert(  col1.next(1,Color::_10) == 6  );
	assert(  col1.next(1,Color::_11) == 5  );

	std::cout << "[OK] next(u,col)\n";


	/* ADVANCED */
	//TODO:
	
	/* correct color from other end */
	for( Vertex_t i=0 ; i<8 ; ++i )
	{
		assert(  col3.otherEnd_color(i,0) == col3[i][0]  );
		assert(  col3.otherEnd_color(i,1) == col3[i][1]  );
		assert(  col3.otherEnd_color(i,2) == col3[i][2]  );
	}
	std::cout << "[OK] otherEnd_color(idv,id)\n";
	//TODO:





	return 0;
}
