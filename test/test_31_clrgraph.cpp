#include <iostream>
#include <cassert>

/* include ba_graph */
#include "../implementation.h"
#include <io.hpp>

/* include our library */
#define DEBUG
#include "../project.hpp"



int main(){

	/* Prepare a coloring */
	const std::string str_twistedCube{"GKh]@c"};
	const CubicMultipole m_twistedCube( str_twistedCube );

	const ColoringGraph clrgraph( m_twistedCube );

	std::cout << str_twistedCube << '\n';
	std::cout << clrgraph.print();
	return 0;
}
