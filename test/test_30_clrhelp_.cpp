#include <iostream>
#include <cassert>

/* include ba_graph */
#include "../implementation.h"
#include <io.hpp>

/* include our library */
#define DEBUG
#include "../project.hpp"



int main(){

	/* Prepare a coloring */
	const std::string str_twistedCube{"GKh]@c"};
	const CubicMultipole m_twistedCube( str_twistedCube );

	std::stringstream ss;
	ss.str(std::string());
	ss  << "8\n"
	    << "1  2  3\n"
	    << "1  3  2\n"
	    << "1  3  2\n"
	    << "1  2  3\n"
 	    << "2  3  1\n"
	    << "3  2  1\n"
	    << "3  2  1\n"
	    << "2  3  1\n";
	Coloring clr( m_twistedCube, ss.str() );

	//std::cout << clr.multipole().print();
	/* TwistedCube:
		8\n"
		3  4  6\n"
		2  5  6\n"
		1  4  7\n"
		0  5  7\n"
		0  2  5\n"
		1  3  4\n"
		0  1  7\n"
		2  3  6\n"
	*/

	std::vector<bool> dummy_prepnute(1ULL<<16,false);
//	dummy_prepnute.resize(1ULL<<16);



	/* test lvl1 */
	Color col[] = {Color::_01,Color::_10};
	assert(  clr(0,3) == Color::_01 );
	assert(  clr(3,5) == Color::_10 );
	assert(  clr(5,4) == Color::_01 );
	assert(  clr(4,0) == Color::_10 );
	// Cycle:  _ 01   10   01   10 
	// 	   0 -> 3 -> 5 -> 4 -> 0 

	  dummy_prepnute.clear();  dummy_prepnute.resize(1ULL<<16);
	assert(  prepni_hranu_a_posun(clr,0,3,col,dummy_prepnute) == 5  );
	assert(  clr(0,3) == Color::_10 );
	 //   10 _ 10   01   10 
	 // 0 -> 3 -> 5 -> 4 -> 0 
	
	  dummy_prepnute.clear();  dummy_prepnute.resize(1ULL<<16);
	assert(  prepni_hranu_a_posun(clr,3,5,col,dummy_prepnute) == 4  );
	assert(  clr(3,5) == Color::_01 );
	 //   10   01 _ 01   10 
	 // 0 -> 3 -> 5 -> 4 -> 0 

	  dummy_prepnute.clear();  dummy_prepnute.resize(1ULL<<16);
	assert(  prepni_hranu_a_posun(clr,5,4,col,dummy_prepnute) == 0  );
	assert(  clr(5,4) == Color::_10 );
	 //   10   01   10 _ 10 
	 // 0 -> 3 -> 5 -> 4 -> 0 
	 
	  dummy_prepnute.clear();
	  dummy_prepnute.resize(1ULL<<16);
	assert(  prepni_hranu_a_posun(clr,4,0,col,dummy_prepnute) == NOT_FOUND  );
	assert(  clr(4,0) == Color::_01 );
	 //   10   01   10   01 _
	 // 0 -> 3 -> 5 -> 4 -> 0 

	assert(  clr(0,3) == Color::_10 );
	assert(  clr(3,5) == Color::_01 );
	assert(  clr(5,4) == Color::_10 );
	assert(  clr(4,0) == Color::_01 );
	// Cycle:    10   01   10   01 
	// 	   0 -> 3 -> 5 -> 4 -> 0 
	

	std::cout << "[OK]  prepni_hranu_a_posun()\n";

	  dummy_prepnute.clear();
	  dummy_prepnute.resize(1ULL<<16);
	assert(  true == prepni_kruznicu(clr,0,3,Color::_01,dummy_prepnute)  );/*unswitch*/
	assert(  clr(0,3) == Color::_01 );
	assert(  clr(3,5) == Color::_10 );
	assert(  clr(5,4) == Color::_01 );
	assert(  clr(4,0) == Color::_10 );
 
	  dummy_prepnute.clear();
	  dummy_prepnute.resize(1ULL<<16);
	assert( false == prepni_kruznicu(clr,5,4,Color::_01,dummy_prepnute)  ); /*same color*/
	  dummy_prepnute.clear();
	  dummy_prepnute.resize(1ULL<<16);
	assert(  true == prepni_kruznicu(clr,5,4,Color::_10,dummy_prepnute)  );/*switch again*/
	assert(  clr(0,3) == Color::_10 );
	assert(  clr(3,5) == Color::_01 );
	assert(  clr(5,4) == Color::_10 );
	assert(  clr(4,0) == Color::_01 );

	  dummy_prepnute.clear();
	  dummy_prepnute.resize(1ULL<<16);
	assert(  true == prepni_kruznicu(clr,4,5,Color::_01,dummy_prepnute)  );/*unswitch*/
	assert(  clr(0,3) == Color::_01 );
	assert(  clr(3,5) == Color::_10 );
	assert(  clr(5,4) == Color::_01 );
	assert(  clr(4,0) == Color::_10 );

	std::cout << "[OK]  prepni_kruznicu(clr,u,v,col1)\n";

	return 0;
}
