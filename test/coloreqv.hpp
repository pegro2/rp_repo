#ifndef COLOREQV
#define COLOREQV

#include <memory>
#include "../project/coloring.hpp"
/**
 * Contains functions that check for similarity between two colorings
 * WARNING:
 * -- ALL functions here (except graph_identical) ASSUME that the two colorings
 *    are colorings of SAME GRAPH, if unsure, run graph_identical()
 *
 *
 * -- for now, used only in tests so that programmer testing only has to
 *    create one coloring for 6 that can be built by simple renaming of colors.
 *
 * -- for now, we think that functions here are too simple to test
 *    code-review is more important here
 */


/*********************** EXPORTED FUNCTION DECLARATIONS ***********************/
typedef void (*Transform)( Coloring& );
/**** Transformation functions ****/
/*  Rename all colors, such that egdes outgoing from vertex 0 are colored
 *  in this order [0]=_01 [1]=_10 [2]=_11 */
void normalize( Coloring& );



/* (*Transform)()s given Colorings then runs color_identical(c1,c2) on them
 * `_with` version only runs (*Transform)() on first argument */
/* default= "if switching colors makes them identical, they are equivalent" */
bool
color_equivalent( const Coloring&, const Coloring&, Transform f=&normalize );
bool
color_equivalent_with( const Coloring&, const Coloring&, Transform f=&normalize );

/* both check that colorings (colors|graph) is the EXACT copy of the other */
bool color_identical( const Coloring&, const Coloring& );
bool graph_identical( const Coloring&, const Coloring& );








/*************************** EXPORTED FUNCTIONS *******************************/
#define TEST(expr) if(!(expr)){return false;}

bool color_equivalent( const Coloring& c1, const Coloring& c2, Transform f )
{
	Coloring coloring1(c1);
	Coloring coloring2(c2);

	(*f)(coloring1);
	(*f)(coloring2);

	return color_identical(coloring1,coloring2);
}
bool color_equivalent_with( const Coloring& c1, const Coloring& c2, Transform f )
{
	Coloring coloring1(c1);

	(*f)(coloring1);

	return color_identical( coloring1,c2 );
}
 //assuming same underlaying graph
//if cannot assume, use are_identical() instead
bool color_identical( const Coloring& c1, const Coloring& c2 )
{
	TEST( c1.order() == c2.order() )

	const size_t order = c1.order();
	for( size_t i=0 ; i<order ; ++i )
	{
		TEST( c1[i][0] == c2[i][0] )
		TEST( c1[i][1] == c2[i][1] )
		TEST( c1[i][2] == c2[i][2] )
	}
	return true;
}

bool graph_identical( const Coloring& c1, const Coloring& c2 )
{
	TEST( c1.order() == c2.order() )
  const size_t order = c1.order();

	const CubicMultipole& m1 = c1.multipole();
	const CubicMultipole& m2 = c2.multipole();
	for( size_t i=0 ; i<order ; ++i )
	{
		TEST( m1[i][0] == m2[i][0] )
		TEST( m1[i][1] == m2[i][1] )
		TEST( m1[i][2] == m2[i][2] )
	}
	return true;
}



/*************** TRANSFORMATION FUNCTIONS ***************/
void normalize( Coloring& c )
{
	const Color old_01 = c[0][0];
	const Color old_10 = c[0][1];
	const Color old_11 = c[0][2];

	const size_t order = c.order();
	for( size_t i=0 ; i<order ; ++i )
	{
		for( size_t j=0 ; j<3 ; ++j )
		{
			const Color col = c[i][j];

			if( old_01 == col ) c[i][j] = Color::_01;
			if( old_10 == col ) c[i][j] = Color::_10;
			if( old_11 == col ) c[i][j] = Color::_11;
		}
	}
}
#undef TEST
#endif
