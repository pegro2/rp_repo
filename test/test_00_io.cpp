#include <iostream>
#include <cassert>

#include "../implementation.h"
#include <io.hpp>

#define DEBUG
#include "../project.hpp"

int main()
{

	std::cout << "[INFO]:"
		<< "\n    "
		<< "sizeof(Vertex_t): " 
		<< sizeof(Vertex_t)
		<< "\n    "
		<< "legal vertex numbers: [" 
		<< (long long) 0
	      	<< "," 
		<< (long long) ( (Vertex_t)(INVALID-1) -2)
		<< "]"
		<< "\n    "
		<< "legal outgoing (negative) numbers: [" 
		<< (long long) ENCODE_OUTGOING(0) 
		<< "," 
		<< (long long) ENCODE_OUTGOING( (Vertex_t)(INVALID-1) -2)
		<< "]"
		<< "\n    "
		<< "INVALID: " 
		<< (long long) INVALID
		<< std::endl;

	/*test from ba_graph -- verify ba_graph::read_graph6()*/
	std::string s1 = "DQc";
	ba_graph::Graph g1( ba_graph::read_graph6(s1.begin(), s1.end()) );
	assert(print_nice(g1) == "0: 2 4\n1: 3\n2: 0\n3: 1 4\n4: 0 3\n");


	/*cubic graph girth 3 vertices 10 -- https://hog.grinvin.org/Cubic */
	std::string s10_3 = "I{O_ooE@W";
	CubicMultipole m10_3( ba_graph::read_graph6(s10_3.begin(), s10_3.end()) );


	/*cubic graph girth 4 vertices 12 -- https://hog.grinvin.org/Cubic */
	std::string s12_4 = "K?hY@eOGKCGB";
	CubicMultipole m12_4( ba_graph::read_graph6(s12_4.begin(), s12_4.end()) );

	/*cubic graph girth 5 vertices 12 -- https://hog.grinvin.org/Cubic */
	std::string s12_5 = "KsP@PGWCOH?R";
	CubicMultipole m12_5( ba_graph::read_graph6(s12_5.begin(), s12_5.end()) );


	/*cubic graph girth 4 vertices 16 -- https://hog.grinvin.org/Cubic */
	std::string s16_4 = "OCHQ@CAGKO?B_GO@aG?W@";
	CubicMultipole m16_4( ba_graph::read_graph6(s16_4.begin(), s16_4.end()) );

	/*cubic graph girth 5 vertices 16 -- https://hog.grinvin.org/Cubic */
	CubicMultipole m16_5( "OsP@PGWC?G?Q?O?G_BO?Y" );


	CubicMultipole twistedCube( "GKh]@c");

	std::cout << "[OK] Cubic graph -- reading\n";

	return 0;
}
