#include <cstddef>

#ifdef DEBUG
#include <cassert>
#endif

#include "implementation.h"
#include "types.hpp"

#include "multipol.hpp"

// HELPER FUNC
static Vertex_t connected_to_via( Vertex_t, const ba_graph::Incidence& );

CubicMultipole::CubicMultipole( ba_graph::Graph g )
{
	//load graph
	size_t order = g.order();
	std::vector< std::array<Vertex_t,3> > adjLists( order ); /*Nx3 2Darray*/


	is_graph_ = true;
	/********** PRECONSTRUCT *********/
	for( ba_graph::Rotation &r : g )
	{
		if( 3 == r.degree() )
		{
			const Vertex_t n = r.n().to_int();

			adjLists[n][0] = connected_to_via(n,r[0]);
			adjLists[n][1] = connected_to_via(n,r[1]);
			adjLists[n][2] = connected_to_via(n,r[2]);

			//check: ensure NO duplicate vertices/edges and NO loop
			if( adjLists[n][0] == adjLists[n][1]
			||  adjLists[n][0] == adjLists[n][2]
			||  adjLists[n][1] == adjLists[n][2]
			||  adjLists[n][0] == n
			||  adjLists[n][1] == n
			||  adjLists[n][2] == n
			){
				throw "Invalid: Multipole Not Cubic";
			}
		}
		else if( 1 == r.degree() ){
			;/*will be handled in next run*/
		}
		else
		{
			throw "Invalid: unexpected degree";
		}
	}
	/**
	 * Now, every cubic vertex is connected with other cubic vertices
	 * as in `g`;
	 * HOWEVER some vertices are still connected with 1-degree vertices
	 * 	** "Multipole" edges not yet handled **
	 */

	for( ba_graph::Rotation &r : g )
	{
		if( 1 == r.degree() )
		{
			const Vertex_t n = r.n().to_int();
#ifdef DEBUG
			assert(  n != 0  );
#endif

			//check
			if( adjLists[n][0] == INVALID ){
				throw "Invalid: two 1-degree edges connected";
			}


			const Vertex_t n_other = connected_to_via(n,r[0]);

			int8_t other_id=0;
			if( adjLists[n_other][0] == n ) other_id = 0;
			if( adjLists[n_other][1] == n ) other_id = 1;
			if( adjLists[n_other][2] == n ) other_id = 2;

			/* MULTIPOLE_OUTGOING is negative vertex
			 * encoded(n) so we can uniquely identify them */
			adjLists[n_other][other_id] = ENCODE_OUTGOING(n);

			adjLists[n][0] = INVALID;

			is_graph_ = false;
		}
	}
	/**
	 * Now, every cubic vertex is connected with
	 * either other cubic vertices,
	 *     or with MULTIPOLE_OUTGOING -n
	 *
	 * 1-degree vertices are present but connected only to "INVALID"
	 */



	/**************** CONSTRUCTION ****************/

	/*count true order of multipole (not counting side-edges)*/
	order_ = 0;
	for( auto it=std::begin(g); it!=std::end(g) ; it++ )
	{
		ba_graph::Rotation& r = *it;
		if( r.degree()==3 ) ++order_;
	}

	/* Build the multipole/graph -- COPYING only 3-degree vertices*/
	adjLists_.resize( order_ );

	size_t i=0,j=0;
	while(1)
	{
		/* skip 1-degree vertices (connected to INVALID) */
		while( j < order && adjLists[j][0] == INVALID )
			++j;

		if( j == order )
		   	break;

		adjLists_[i][0] = adjLists[j][0];
		adjLists_[i][1] = adjLists[j][1];
		adjLists_[i][2] = adjLists[j][2];

		++i;
		++j;
	}
}

inline bool
CubicMultipole::is_graph() const { return is_graph_; }

inline size_t
CubicMultipole::order() const { return order_; }

inline const std::array<Vertex_t,3>&
CubicMultipole::operator[]( Vertex_t v ) const
{
#ifdef DEBUG
	assert( ( 0 <= v  &&  ((size_t) v) < order_)  );
#endif
	return adjLists_[v];
}
inline bool
CubicMultipole::connected( Vertex_t u, Vertex_t v ) const
{
#ifdef DEBUG
	assert( ( 0 <= u  &&  ((size_t) u) < order_)  );
#endif

	bool connected_uv=false;
	connected_uv |= ( adjLists_[u][0] == v );
	connected_uv |= ( adjLists_[u][1] == v );
	connected_uv |= ( adjLists_[u][2] == v );
#ifdef DEBUG
	bool connected_vu=false;
	connected_vu |= ( adjLists_[v][0] == u );
	connected_vu |= ( adjLists_[v][1] == u );
	connected_vu |= ( adjLists_[v][2] == u );
	assert(  connected_uv == connected_vu  );
#endif
	return connected_uv;
}


inline int8_t
CubicMultipole::otherEnd_id(Vertex_t idv, int8_t vertex_id ) const
{
#ifdef DEBUG
	assert(  !IS_MULTIPOLE_OUTGOING( idv )  );
#endif


	/* (*this)[x] ... effectively invokes multipole_[](x) */
	Vertex_t other_vertex = (*this)[idv][vertex_id];

	if(  IS_MULTIPOLE_OUTGOING(other_vertex)  )
		return -1;


	const std::array<Vertex_t,3>& vs = (*(this))[other_vertex];

	if( vs[0] == idv ) return 0;
	if( vs[1] == idv ) return 1;
	if( vs[2] == idv ) return 2;

	return NOT_FOUND;
}



/********************************* printing **********************************/
inline std::string
CubicMultipole::print() const
{
	std::stringstream ss;
	ss << order_ << '\n';
	for( size_t i=0 ; i<order_ ; ++i )
	{
		/*have to cast because c++ treats uint8_t as char*/
		ss //<< i << " : "
		   << (int) adjLists_[i][0] << "  "
		   << (int) adjLists_[i][1] << "  "
		   << (int) adjLists_[i][2]
		   << '\n'; /* double space because readability*/
	}
	return ss.str();
}



/**** **** ****   HELPER FUNCTIONS  **** **** ****/
static Vertex_t
connected_to_via( Vertex_t n, const ba_graph::Incidence& i )
{
	if( n == i.n1().to_int() )
		return i.n2().to_int();
	else
		return i.n1().to_int();
}
/**** **** **** END HELPER FUNCTIONS **** **** ****/
