#ifndef RP_TYPES_HPP
#define RP_TYPES_HPP

#include <cstdint>
#include "implementation.h"

enum Color:std::uint8_t { nil=0, _01=1, _10=2, _11=3 };

/********************************** TYPES *************************************/
/*    [0 - (TYPE_MAX-2)]  are legal 'vertex' numbers
 * connected to `-(n+2)` means that it is connecting to OUTSIDE world
 * `TYPE_MIN` is INVALID */
//typedef int8_t Vertex_t; // legal:[0-125] invalid:-128 negative:[(-127)-(-2)]

typedef int16_t Vertex_t; //for testing purposes try bigger Vertex_t



/******************************** CONSTANTS ***********************************/
constexpr Vertex_t INVALID = (Vertex_t) (1ULL << (sizeof(Vertex_t)*8-1));/*MIN*/

//SHALL be -1  ...  //must fit into int8_t and Vertex_t
#define NOT_FOUND -1



/********************************** MACROS ************************************/
#define IS_MULTIPOLE_OUTGOING(n) ((n)<0)
#define ENCODE_OUTGOING(n) ( -(n+2) )
#define DECODE_OUTGOING(n) ( -(n+2) )
// encodes/decodes outgoing edge, so that you now which one it originally was
// [ 
//   multipole given as g6string with order1 verticies 
//   => find which original vertex it was 
// ]

#endif
