#include <sstream>
#include <iomanip> //std::setw
#include <utility>

#include "colorgen.hpp"
#include "types.hpp"

#include "clrgraph.hpp"
#include "clrhelp_.cpp"

/************************ GENERATE GRAPH OF COLORINGS *************************/
ColoringGraph::ColoringGraph( const CubicMultipole& m )
{
	multipole_ = new CubicMultipole(m); //it's pointer because we want CONST

	/* compute translation maps 
	 * no to coloring  -- O(1) time
	 * coloring to no  -- O(lg n) time  //but comparing colorings which 
	 *   if different, are REALLY different so comparation will end fast*/
	colorings = all_colorings( *multipole_ );

	order_ = colorings->size();
	for( ColoringNo no=0 ; no<order_ ; ++no )
	{
		coloring_inverse_map[ (*colorings)[no] ] = no;
	}


	/* end prematurely if nothing to do */
	if( order_ == 0 )
	{
		return;
	}






	/* prepare support data-structures */
	const size_t N = multipole_->order();

	std::vector<bool> prepnute; //for every[u,v,col]: ci prepnute (cyklus)
	adjSet_.resize( colorings->size() );
	adjList_.resize( colorings->size() );


	/* colorings graph creation */
	for( size_t i=0 ;  i < order_  ; ++i )
	{
		const Coloring& clr = (*colorings)[i];
		const ColoringNo clrNo = this->coloring_to_no(  clr  );

		//
		prepnute.clear();
		prepnute.resize( N*N*3 );

		//  TODO: for every cycle(path), switch it
		//  [SLOW version] ... for every edge and color, switch its cycle(path)
		for( Vertex_t u=0 ;  static_cast<size_t>(u) < N  ; ++u )
		{
			for( Vertex_t v=u+1 ;  static_cast<size_t>(v) < N  ; ++v )
			{
				if(  ! this->multipole().connected(u,v)  )
				{
					continue;
				}


#define PREPNI_A_PRIDAJ(clr,u,v,COLOR) 									\
{													\
				if(  !prepnute[ hashCycle(N,u,v,COLOR) ]  )					\
				{									\
					prepnute[ hashCycle(N,u,v,COLOR) ]=true;				\
													\
					Coloring copy(  clr  );						\
					if(  prepni_kruznicu(copy, u,v,COLOR, prepnute)  ) 			\
					{								\
						ColoringNo copyNo = this->coloring_to_no( copy );	\
						if( ! this->connected(clrNo,copyNo)  )			\
						{							\
							this->adjSet_[ clrNo ].insert( copyNo );	\
							this->adjSet_[ copyNo ].insert( clrNo );	\
							this->adjList_[ clrNo ].push_back( copyNo );	\
							this->adjList_[ copyNo ].push_back( clrNo );	\
						}							\
					}								\
				}									\
}
				//will ignore if switching to same color
				PREPNI_A_PRIDAJ(clr,u,v,Color::_01);
				PREPNI_A_PRIDAJ(clr,u,v,Color::_10);
				PREPNI_A_PRIDAJ(clr,u,v,Color::_11);
			}
		}
	}
	/*
	 * create colorings ... backtrack
	 * => O( #colorings ) ?
	 *
	 * for every coloring ... O(n)
	 *    for every pair of edges check if together ... O(n^2) * O(1)
	 *    for every cycle copy(coloring) a prepni  ... O(n)+O(n)
	 *
	 * => O( #colorings * ( n^2 + #kruznic*(n+n) )
	 * */
#undef PREPNI_A_PRIDAJ
	no_components_ = count_components( adjList_ );
}
inline size_t 
ColoringGraph::order() const 
{
	return order_;
}
inline size_t
ColoringGraph::no_components() const
{
	return no_components_;
}
inline const CubicMultipole&
ColoringGraph::multipole() const
{
	return *multipole_;
}
inline const std::vector<ColoringNo>&
ColoringGraph::operator[]( size_t index ) const
{
	return adjList_[ index ];
}
inline bool
ColoringGraph::connected( ColoringNo no1, ColoringNo no2 ) const
{
#ifdef DEBUG
	bool found12=(  adjSet_[no1].find(no2) != adjSet_[no1].end()  );
	bool found21=(  adjSet_[no2].find(no1) != adjSet_[no2].end()  );
	assert(  found12 == found21  );
#endif
	return (  adjSet_[no1].find( no2 ) != adjSet_[no1].end()  );
}


inline const Coloring&
ColoringGraph::no_to_coloring( ColoringNo number ) const
{
	return (*colorings)[ number ];
}
inline const ColoringNo&
ColoringGraph::coloring_to_no( const Coloring& col ) const
{
	return coloring_inverse_map.at( col );
}


inline std::string
ColoringGraph::print() const
{
	std::stringstream ss;
	ss << "GRAPH OF COLORINGS\n";
	ss << "#COMPONENTS=" << this->no_components() << '\n';
	ss << this->print_graph();
	ss << "END\n";


	// print colorings
	for( size_t i=0;  i<order_  ; ++i )
	{
		ss << "COLORING #" << i << '\n';
		ss << (*(this->colorings))[i].print();
		ss << "END\n";
	}


	// UNDERLAYING GRAPH | MULTIPOLE
	ss << "UNDERLAYING " << (this->multipole().is_graph() ? "GRAPH" : "MULTIPOLE") << '\n';
	ss << this->multipole().print();
	ss << "END\n";


	return ss.str();
}
inline std::string
ColoringGraph::print_graph() const //has extra " " at end of every line!
{
	std::stringstream ss;

	ss << std::setw(3) << order_ << "\n";
	for(size_t i=0 ;  i<order_  ; ++i)
	{
		//const size_t N = adjList_[i].size();
		//for( size_t j=0 ;  j<N  ; ++j ) //actual (creation) order
		//{
		//	ss << std::setw(3) << adjList_[i][j] << " ";
		//}
		for( const ColoringNo& neighbour : adjSet_[i] ) //sorted order
		{
			ss << std::setw(3) << neighbour << ' ';
		}

		ss << '\n';
	}

	return ss.str();
}
