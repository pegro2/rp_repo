#ifndef MULTIPOL_HPP
#define MULTIPOL_HPP

#include <cstddef>
#include <vector>
#include <array>

#include "implementation.h"
#include "types.hpp"

/**
 * Class representing a Graph or Multipole(graph with OUTGOING edges)
 * represented via adjacent lists
 * 	since we think about cubic graphs only, Nx3 2D array is ideal
 *
 * NOTE: this class is should be always read-only (const)
 **/

/* !!!
 * WE REQUIRE THAT VERTEX no 0 must NOT be MULTIPOLE_OUTGOING (with order 1)
 *   -- just rename the graph if causing problem
 **/
class CubicMultipole {
	bool is_graph_; /* true if cubic graph */

	size_t order_;
	std::vector< std::array<Vertex_t,3> > adjLists_; /* N*3 2Darray*/
public:
	CubicMultipole( ba_graph::Graph );
	CubicMultipole( std::string g6 )
		: CubicMultipole( ba_graph::read_graph6(g6.begin(),g6.end()) ){}
	inline bool is_graph() const;

	inline size_t order() const;
	inline const std::array<Vertex_t,3>& operator[]( Vertex_t ) const;
	inline bool  connected( Vertex_t, Vertex_t ) const;

	/* printing */
	inline std::string print() const;


	/* ADVANCED functionality */
	inline int8_t otherEnd_id( Vertex_t idv, int8_t vertex_id ) const;
};
#endif
