#include <sstream>

#ifdef DEBUG
#include <cassert>
#endif

#include "coloring.hpp"

/*************************** FUNCTION DEFINITONS ******************************/
Coloring::Coloring( const CubicMultipole& m )
{
	order_ = m.order();
	multipole_ = &m;
	colLists_.resize( order_ );

	/*deep copy and set Color::nil*/
	for( size_t i=0 ; i < order_ ; ++i )
	{
		colLists_[i][0] = Color::nil;
		colLists_[i][1] = Color::nil;
		colLists_[i][2] = Color::nil;
	}
}
Coloring::Coloring( const Coloring& c )
{
	/*deep copy all*/
	order_ = c.order_;
	multipole_ = c.multipole_;
	colLists_.resize( order_ );

	for( size_t i=0 ; i < order_ ; ++i )
	{
		colLists_[i][0] = c.colLists_[i][0];
		colLists_[i][1] = c.colLists_[i][1];
		colLists_[i][2] = c.colLists_[i][2];
	}
}
Coloring::Coloring( const CubicMultipole& m, std::string ba_literal )
{
	std::stringstream ss;
	ss << ba_literal;

	ss >> order_;
	colLists_.resize( order_ );
	multipole_ = &m;


#ifdef DEBUG
	assert( m.order() == order_ );
#endif


	for( size_t i=0 ; i < order_ ; ++i )
	{
		/**
		 * uint8_t is treated as char -> BAD, we need to read numbers
		 * we expect users to ensure that input size
		 * fits into Vertex_t, Color::underlaying
		 * if NOT, please change types in types.hpp and recompile
		 **/
		unsigned long long tmp;
		ss >> tmp;  colLists_[i][0] = static_cast<Color>(tmp);
		ss >> tmp;  colLists_[i][1] = static_cast<Color>(tmp);
		ss >> tmp;  colLists_[i][2] = static_cast<Color>(tmp);
	}
}
#ifdef DUMMY_COLORING
Coloring::Coloring( std::string ba_literal )
{
	std::stringstream ss;
	ss << ba_literal;

	ss >> order_;
	colLists_.resize( order_ );
	multipole_ = nullptr;

	for( size_t i=0 ; i < order_ ; ++i )
	{
		/**
		 * uint8_t is treated as char -> BAD, we need to read numbers
		 * we expect users to ensure that input size
		 * fits into Vertex_t, Color:underlaying
		 * if NOT, please change types in types.hpp and recompile
		 **/
		unsigned long long tmp;
		ss >> tmp;  colLists_[i][0] = static_cast<Color>(tmp);
		ss >> tmp;  colLists_[i][1] = static_cast<Color>(tmp);
		ss >> tmp;  colLists_[i][2] = static_cast<Color>(tmp);
	}
}
#endif





inline size_t
Coloring::order() const { return order_; }

inline const CubicMultipole&
Coloring::multipole() const { return *multipole_; }



inline const std::array<Color,3>&
Coloring::operator[]( Vertex_t v ) const
{
#ifdef DEBUG
	assert( ( 0 <= v  &&  ((size_t) v) < order_)  );
#endif
	return colLists_[v];
}
inline std::array<Color,3>&
Coloring::operator[]( Vertex_t v ) /* MUTABLE COUNTERPART */
{
#ifdef DEBUG
	assert( ( 0 <= v  &&  ((size_t) v) < order_)  );
#endif
	return colLists_[v];
}



inline const Color&
Coloring::operator()( const Vertex_t u, const Vertex_t v ) const
{
#ifdef DEBUG
	assert(  !(IS_MULTIPOLE_OUTGOING(u) && IS_MULTIPOLE_OUTGOING(v))  ); //not both
#endif
	if( IS_MULTIPOLE_OUTGOING(u) )
	{
		return (*this)(v,u);
	}


	int8_t v_id = nextID_via_vertex( u, v );
#ifdef DEBUG
	assert(  NOT_FOUND != v_id  );
#else
	if( NOT_FOUND == v_id ) throw NOT_FOUND;
#endif

	return colLists_[u][ v_id ];
}
inline Color&
Coloring::operator()( const Vertex_t u, const Vertex_t v ) /* mutable */
{
#ifdef DEBUG
	assert(  !(IS_MULTIPOLE_OUTGOING(u) && IS_MULTIPOLE_OUTGOING(v))  ); //not both
#endif
	if( IS_MULTIPOLE_OUTGOING(u) )
	{
		return (*this)(v,u);
	}


	int8_t v_id = nextID_via_vertex( u, v );
#ifdef DEBUG
	assert(  NOT_FOUND != v_id  );
#else
	if( NOT_FOUND == v_id ) throw NOT_FOUND;
#endif

	return colLists_[u][ v_id ];
}



inline Vertex_t
Coloring::next( const Vertex_t u, const Color col ) const
{
        if(  IS_MULTIPOLE_OUTGOING( u )  )
	{
		return NOT_FOUND;
	}

	int8_t v_id = nextID_via_color( u,col ) ;
	if( NOT_FOUND == v_id )
	{
		return NOT_FOUND;
	}

	return multipole()[u][ v_id ];
}






/********************************* PRINT **********************************/
inline std::string
Coloring::print() const
{
	std::stringstream ss;
	ss << order_ << '\n';
	for( size_t i=0 ; i<order_ ; ++i )
	{
		/*have to cast because c++ treats uint8_t as char*/
		ss //<< i << " : "
		   << (int) colLists_[i][0] << "  "
		   << (int) colLists_[i][1] << "  "
		   << (int) colLists_[i][2] 
		   << '\n'; /* double space because readability*/
	}
	return ss.str();
}



/********************************* OPERATORS **********************************/
bool operator< ( const Coloring& lhs, const Coloring& rhs )
{
	return lhs.colLists_ <  rhs.colLists_;
}
bool operator==( const Coloring& lhs, const Coloring& rhs )
{
	return lhs.colLists_ == rhs.colLists_;
}



/********************************* ADVANCED ***********************************/
inline Color
Coloring::otherEnd_color(Vertex_t idv, int8_t vertex_id) const
{
#ifdef DEBUG
        assert(  !IS_MULTIPOLE_OUTGOING( idv )  );
#endif


	//Vertex_t other_vertex = (*(this->multipole_))[idv][vertex_id];
	Vertex_t other_vertex = multipole()[idv][vertex_id];
        if( IS_MULTIPOLE_OUTGOING(other_vertex) )
                return Color::nil;

	int8_t idv_id = nextID_via_vertex( other_vertex, idv );
	if( NOT_FOUND == idv_id )
		throw NOT_FOUND;

	return colLists_[ other_vertex ][ idv_id ];
}
inline int8_t
Coloring::otherEnd_id(Vertex_t idv, int8_t vertex_id ) const
{
	//return multipole_->otherEnd_id(idv,vertex_id);
	return multipole().otherEnd_id(idv,vertex_id);
}



inline int8_t
Coloring::nextID_via_vertex( const Vertex_t u, const Vertex_t v) const 
{
#ifdef DEBUG
	assert(  !IS_MULTIPOLE_OUTGOING(u)  );
#endif


        //const std::array<Vertex_t,3>& adjVertices = (*multipole_)[ u ];
        const std::array<Vertex_t,3>& adjVertices = multipole()[ u ];

        if( adjVertices[0] == v ) return 0;
        if( adjVertices[1] == v ) return 1;
        if( adjVertices[2] == v ) return 2;

	return NOT_FOUND;
}
inline int8_t
Coloring::nextID_via_color( const Vertex_t u, const Color col) const
{
#ifdef DEBUG
	assert(  !IS_MULTIPOLE_OUTGOING(u)  );
#endif


        const std::array<Color,3>& adjColors = (*this)[ u ];

        if( adjColors[0] == col ) return 0;
        if( adjColors[1] == col ) return 1;
        if( adjColors[2] == col ) return 2;

	return NOT_FOUND;
}
