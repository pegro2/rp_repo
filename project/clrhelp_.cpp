#ifndef CLRHELP__CPP
#define CLRHELP__CPP


/********************************* EXPORTED ***********************************/
/*  Expects valid coloring(with valid multipole) as input,
 *  expects u,v to be connected
 * aborts instantly if Coloring(u,v) == Color */
bool
prepni_kruznicu( Coloring&, Vertex_t, Vertex_t, Color, std::vector<bool>& );

Vertex_t
prepni_hranu_a_posun( Coloring&, Vertex_t, Vertex_t, const Color[2], std::vector<bool>& );

size_t
count_components( const std::vector<std::vector<ColoringNo>>& adjList );

inline size_t
hashCycle(size_t N, Vertex_t u, Vertex_t v, Color col);
/********************************** STATIC ************************************/
static size_t
dfs( const std::vector<std::vector<ColoringNo>>& adjList, ColoringNo i, std::vector<bool>& visited );





/***************************** IMPLEMENTATION *********************************/
Vertex_t
prepni_hranu_a_posun( Coloring& clr, Vertex_t u, Vertex_t v, const Color colors[2],
		std::vector<bool>& prepnute )
{
	const Color& old_col = clr(u,v);
	Color next_col;
	if(  old_col == colors[0] ) next_col = colors[1];
	else 
	 if( old_col == colors[1] ) next_col = colors[0]; 
	 else 
          throw "Invalid coloring detected";


	//find where to go next (maybe NOT_FOUND)
	Vertex_t next_v = clr.next(v,next_col);


	clr(u,v) = next_col;
	clr(v,u) = next_col;
	prepnute[ hashCycle( clr.multipole().order(), u,v,next_col ) ]=true;


	return next_v;
}
bool
prepni_kruznicu( Coloring& clr, const Vertex_t u, const Vertex_t v, Color col1,
		std::vector<bool>& prepnute )
{
	const Color colors[2] = { clr(u,v), col1 };
	if( colors[0] == colors[1] )
	{
		return false;
	}


	const Vertex_t next_u = clr.next(u,col1);
#ifdef DEBUG
	assert( next_u == NOT_FOUND  ||  clr(u,next_u) == col1 );
#endif


#define PREPNI_KRUZNICU(u,v)						      		\
{									      		\
	Vertex_t next, _u=u, _v=v;					      		\
	while( NOT_FOUND != (next=prepni_hranu_a_posun(clr, _u,_v,colors, prepnute)) ) 	\
	{										\
		_u = _v;						      		\
		_v = next;						      		\
	}								      		\
}


	//prepne kruznicu
	PREPNI_KRUZNICU(u,v);

	//ak to nebola kruznica(farba nezmenena) ani cela cesta
	//prepni aj druhu cast cesty
	//TODO: test this for multipoles
	if(  (next_u != NOT_FOUND)  &&  (clr(u,next_u) == col1)  )
	{
		PREPNI_KRUZNICU(u,next_u);
	}
	return true;
#undef PREPNI_KRUZNICU
}



size_t
count_components( const std::vector<std::vector<ColoringNo>>& adjList )
{
	/* Count number of components...
	 *  [ DFS that returns #vertices found ]
	 *  run this DFS on every vertex(coloring);
	 *  count number of nonzero returns
	 * */
	const size_t N = adjList.size();
	std::vector<bool> visited;
	visited.resize( N );


	size_t no_components = 0;
	for( ColoringNo i=0;  i<N  ; ++i )
	{
		if( 0 != dfs(adjList,i,visited) )
		{
			++no_components;
		}
	}
	return no_components;
}
static size_t
dfs( const std::vector<std::vector<ColoringNo>>& adjList,
		ColoringNo i, std::vector<bool>& visited )
{
	size_t count=0;
	if( !visited[i] )
	{
		visited[i]=true;
		++count;

		for( const ColoringNo& neighbour : adjList[i] )
		{
			count += dfs(adjList,neighbour,visited);
		}
	}
	return count;
}

/******************************** HELPER **************************************/
inline size_t hashCycle(size_t N, Vertex_t u, Vertex_t v, Color col)
{
	return u*N*3 + v*3 + (static_cast<size_t>(col)-1);
}
#endif
