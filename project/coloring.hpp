#ifndef RP_COLORING_HPP
#define RP_COLORING_HPP

#include "types.hpp"
#include "multipol.hpp"

/**
 * Class `Coloring` represents one specific coloring
 * represented same as CubicMultipole, Nx3 2Darra
 * but adjacent lists have information: color of connection
 *
 * information about color of edge is duplicated
 * (3-4 are connected, 3->4 remembers color X and 4->3 remembers color X)
 * but it actually makes sense in algorithm creating all colorings.
 *
 *
 *
 * ba_literal is following format (for coloring of cubic graph):
 * line0     :  N
 * line1 to N:  col1 col2 col3
 *
 * where on line0: n is (number of vertices)
 * where on lineX: col1 is color for connection with v1 
 * 		   v1 is vertex, such that vertexX is connected to 
 * 		   via i-th connection
 *
 *
 * WARNING:
 *    functions next(),nextID_via_color()  work only on legal colorings
 *    (from 1 vertex exactly 3 diff colored edges)
 *
 *    operator()(u,v) works only on LEGAL edges (present)
 *    (undefined behaviour on u->v not present)
 *
 *    operator()(u,v) and next(u,col) accepts MULTIPOLE_OUTGOING as Vertex_t,
 *    (other functions EXPECT legal vertices)
 **/

/****************************** DECLARATIONS **********************************/
class Coloring{
	size_t order_;
	const CubicMultipole* multipole_; // the underlaying multipole
	//std::vector< std::array<Vertex_t,3> > adjLists_; /* Nx3 2Darray */
	std::vector< std::array<Color,3> > colLists_; /* Nx3 2Darray */

public:
	Coloring( const CubicMultipole& );
	Coloring( const Coloring& );
	Coloring( const CubicMultipole& m, std::string ba_literal ); 
	// does not check if legal coloring
#ifdef DUMMY_COLORING
	Coloring( std::string ba_literal ); /*only for ops:  ==| <|[]|print() */
#endif

	inline size_t order() const;
	//inline const CubicMultipole& graph() const;
	inline const CubicMultipole& multipole() const; /* not in DUMMY */


	/* access COLOR via adjacency list (faster) */
	inline const std::array<Color,3>& operator[]( Vertex_t ) const;
	inline       std::array<Color,3>& operator[]( Vertex_t );

	/* operators */
	friend bool operator< ( const Coloring& lhs, const Coloring& rhs );
	friend bool operator==( const Coloring& lhs, const Coloring& rhs );


	/* printing */
	inline std::string print() const;




	// ADVANCED functionality 
	// -- slower but sophisticated 

	/* access COLOR via edge u->v [or throws NOT_FOUND]*/
	inline const Color& operator()( const Vertex_t, const Vertex_t ) const;
	inline       Color& operator()( const Vertex_t, const Vertex_t );

	/* return next Vertex connected via Color [or return  NOT_FOUND]*/
	inline Vertex_t next( const Vertex_t, const Color) const;




	// function easing usage of operator[]
	// if otherEnd IS_MULTIPOLE_OUTGOING() 	-->  returns -1|Color::nil 
	// if next not found  			-->  throws "NOT_FOUND"
	inline Color  otherEnd_color(Vertex_t idv, int8_t vertex_id) const;
	inline int8_t otherEnd_id(Vertex_t idv, int8_t vertex_id ) const;

	inline int8_t nextID_via_vertex( const Vertex_t, const Vertex_t) const;
	inline int8_t nextID_via_color ( const Vertex_t, const Color) const;
};
#endif
