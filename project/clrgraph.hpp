#ifndef CLRGRAPH
#define CLRGRAPH

#include "coloring.hpp"

#include <map>
#include <set>

/*
 * Graph of Colorings of Multipole_
 * All work will be done in Constructor
 *
 * functions taking Coloring as an argument can take DUMMY_COLORING.
 *
 * NOTE: this class is should always be read-only (const)
 */
using ColoringNo = size_t;
class ColoringGraph { //graph of colorings of multipole(or graph)
	const CubicMultipole* multipole_;  //underlaying multipole

	std::unique_ptr< const std::vector<Coloring> >	colorings;
	std::map<Coloring,ColoringNo> coloring_inverse_map;

	size_t order_=0;
	size_t no_components_=0;
	std::vector< std::vector< ColoringNo > >  adjList_;
	std::vector< std::set< ColoringNo > >  adjSet_; //duplicated info
    public:
	ColoringGraph( const CubicMultipole& );
	~ColoringGraph() { delete multipole_; };

	/* graph */
	inline size_t order() const;
	inline size_t no_components() const;
	inline const CubicMultipole&  multipole() const;

	inline const std::vector<ColoringNo>&  operator[]( size_t ) const;
	inline bool  connected( ColoringNo, ColoringNo ) const;


	/* translation */
	inline const Coloring&	 no_to_coloring( ColoringNo number ) const;
	inline const ColoringNo& coloring_to_no( const Coloring& ) const;


        /* printing */
        inline std::string print() const; // also prints every coloring, underlaying multipole
        inline std::string print_graph() const; //extra " " at end of every line!
};
#endif
