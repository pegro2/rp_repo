#ifndef RP_COLORGEN_H
#define RP_COLORGEN_H

#include <memory>

#include "types.hpp"
#include "coloring.hpp"

/**
 * Generates all_colorings of given cubic graph/mutipole
 * algorithm works in backtrack fashion
 * Tries to color vertex with all permutations of colors 01, 10, 11
 * 	does this recursively for all vertices
 */
std::unique_ptr< const std::vector<Coloring>>
all_colorings( const CubicMultipole& multipole );






/**************************** STATIC DECLARATIONS *****************************/
static constexpr std::array< std::array<Color,3> ,6> color_permutations{{
	{{ Color::_01, Color::_10, Color::_11 }},
	{{ Color::_01, Color::_11, Color::_10 }},

	{{ Color::_10, Color::_01, Color::_11 }},
	{{ Color::_10, Color::_11, Color::_01 }},

	{{ Color::_11, Color::_01, Color::_10 }},
	{{ Color::_11, Color::_10, Color::_01 }},
}}; //{{val}} because initializing (outer braces) and passing C array(inner)

static void dfs( std::vector<Coloring>&, Vertex_t, Coloring);






/**************************** EXPORTED FUNCTION *******************************/
std::unique_ptr< const std::vector<Coloring>>
all_colorings( const CubicMultipole& multipole )
{
	std::vector<Coloring> *colorings = new std::vector<Coloring>;

	Coloring c(multipole);
	dfs(*colorings,0,c);

	//
	// create unique pointer and transfer ownership to it
	//
	std::unique_ptr< const std::vector<Coloring> > uptr;
	uptr.reset( colorings );

	return uptr;
}


/***************************** STATIC FUNCTIONS *******************************/
static void
dfs( std::vector<Coloring>& colorings, Vertex_t idv, Coloring c )
{
	/*recursive end -- and the coloring IS viable*/
	if( 0 <= idv && ((size_t) idv) == c.order() )
	{
		colorings.push_back(c);
		return;
	}

	const Color otherEnd0_color = c.otherEnd_color(idv,0);
	const Color otherEnd1_color = c.otherEnd_color(idv,1);
	const Color otherEnd2_color = c.otherEnd_color(idv,2);

	for( auto permutation : color_permutations)
	{
		/*either not yet assigned color, or the same one assigned*/
		if( otherEnd0_color != Color::nil
		&&  otherEnd0_color != permutation[0] )
			continue;

		if( otherEnd1_color != Color::nil
		&&  otherEnd1_color != permutation[1] )
			continue;

		if( otherEnd2_color != Color::nil
		&&  otherEnd2_color != permutation[2] )
			continue;

		/*this color_permutation is valid*/
		c[idv][0] = permutation[0];
		c[idv][1] = permutation[1];
		c[idv][2] = permutation[2];

		dfs(colorings, idv+1, c); //recursive

		c[idv][0] = Color::nil;
		c[idv][1] = Color::nil;
		c[idv][2] = Color::nil;
	}
	return;
}
#endif
